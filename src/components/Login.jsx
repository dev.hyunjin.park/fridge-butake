import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Input, RadiusButton } from "./commonStyles";

const Background = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: ${(props) => props.theme.black};
`;
const Form = styled.form`
  width: 100%;
  min-height: 40%;
  padding: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;
const InputContainer = styled.div`
  background-color: ${(props) => props.theme.lightGray};
  border-radius: 15px;
  padding: 2px 3px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;
const Login = () => {
  const [activeInput, setActiveInput] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isValid, setIsValid] = useState(false);
  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsValid(true);
      return;
    }
    setIsValid(false);
  }, [email, password]);
  const handleInputClick = (inputId) => {
    setActiveInput(inputId);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    // TO DO: login
    // redirect
  };
  return (
    <Background>
      <Form onSubmit={handleSubmit}>
        <InputContainer>
          <Input
            type="text"
            name="email"
            id="email"
            placeholder="이메일"
            active={activeInput === 1}
            onClick={() => handleInputClick(1)}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </InputContainer>
        <InputContainer>
          <Input
            type="password"
            name="password"
            id="password"
            placeholder="비밀번호"
            active={activeInput === 2}
            onClick={() => handleInputClick(2)}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </InputContainer>
        <RadiusButton isValid={isValid} inputValue="로그인" />
      </Form>
    </Background>
  );
};

export default Login;
