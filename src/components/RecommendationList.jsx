import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Checkbox } from "./commonStyles";

const recipeData = [
  {
    id: 0,
    name: "바질 토마토 스파게티",
    ingredients: [
      { name: "양파" },
      { name: "토마토" },
      { name: "스파게티" },
      { name: "바질" },
      { name: "마늘" },
      { name: "고추" },
    ],
  },
  {
    id: 1,
    name: "오이 김 파스타",
    ingredients: [
      { name: "오이" },
      { name: "김" },
      { name: "낫또" },
      { name: "스파게티" },
      { name: "간장" },
      { name: "마늘" },
      { name: "고추" },
    ],
  },
  {
    id: 2,
    name: "까르보나라",
    ingredients: [
      { name: "계란" },
      { name: "치즈" },
      { name: "스파게티" },
      { name: "치즈" },
    ],
  },
];
const Table = styled.table`
  width: 100%;
  color: ${({ theme }) => theme.lightGray};
  padding: 0 20px;
`;
const TableHead = styled.thead`
  height: 50px;
`;
const TableBody = styled.tbody``;
const TableRow = styled.tr``;
const TableCell = styled.td`
  border-bottom: 1px solid ${({ theme }) => theme.lightGray};
`;
const ListItem = styled.li`
  &:first-child {
    padding-top: 10px;
    font-size: 0.9rem;
    font-weight: 500;
  }
  &:last-child {
    padding-bottom: 10px;
    opacity: 0.7;
    font-size: 0.8rem;
  }
`;

const RecommendationList = () => {
  return (
    <>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>추천</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {recipeData.map((recipe) => (
            <TableRow key={Math.random() + recipe.name}>
              <TableCell>
                <Link
                  // to={`/recipe/${recipe.id}`}
                  to="/recipe/47950"
                  style={{ display: "inline-block" }}
                >
                  <ListItem>{recipe.name}</ListItem>
                  <ListItem>
                    재료:{" "}
                    {recipe.ingredients.reduce(
                      (accu, curr, index) => {
                        if (index === 0) return ` ${curr.name}`;
                        accu += `, ${curr.name}`;
                        return accu;
                      },
                      [""]
                    )}
                  </ListItem>
                </Link>
              </TableCell>
            </TableRow>
          ))}
          {recipeData.map((recipe) =>
            recipe.ingredients.map((item, index) => (
              <TableRow key={index + "item" + recipe.name}>
                <TableCell>
                  <ListItem>
                    <Checkbox item={item} />
                  </ListItem>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </>
  );
};

export default RecommendationList;
