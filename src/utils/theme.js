export const theme = {
  black: "#2B2B2B",
  white: "E2E2E2",
  lightGray: "#efefef",
  purple: "#B2AEE2",
  yellow: "#E0D276",
  orange: "#BE6638",
  pink: "#C5A4C5",
  green: "#317349",
  blue: "#3476C5",
};
