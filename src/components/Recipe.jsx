import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import axios from "axios";

const Recipe = () => {
  const [data, setData] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      const options = {
        method: "GET",
        url: `https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/${id}/information`,
        headers: {
          "X-RapidAPI-Key": process.env.REACT_APP_RECIPE_API_KEY,
          "X-RapidAPI-Host": process.env.REACT_APP_RECIPE_HOST_KEY,
        },
      };

      try {
        const response = await axios.request(options);
        console.log(response.data);
        setData(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, [id]);

  return <div>{data && data.id}</div>;
};

export default Recipe;
