import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faBasketShopping } from "@fortawesome/free-solid-svg-icons";

const Nav = styled.nav`
  width: 100%;
  height: 40px;
  position: fixed;
  top: 0;
  background-color: ${({ theme }) => theme.black};
  padding: 0 10px;
`;
const NavButtonConatiner = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: end;
  padding: 10px;
  background-color: ${({ theme }) => theme.black};
  color: ${({ theme }) => theme.lightGray};
  font-weight: 800;
  font-size: 1.5rem;
  border-bottom: 0.1px solid ${({ theme }) => theme.lightGray};
`;
const List = styled.ul`
  display: flex;
  align-items: center;
  justify-content: end;
  height: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.lightGray};
`;
const ListItem = styled.li`
  color: ${({ theme }) => theme.lightGray};
  font-family: "Gasoek One", cursive;
  font-size: 1.2rem;
  font-weight: 800;
  margin-right: 0.4rem;
`;

const Navigation = () => {
  // TO DO : loggedIn User
  const loggedInUser = true;
  const [isNavBtnClicked, setIsNavBtnClicked] = useState(false);

  return (
    <Nav>
      {!isNavBtnClicked && (
        <NavButtonConatiner
          onClick={(e) => setIsNavBtnClicked((current) => !current)}
        >
          <FontAwesomeIcon icon={faBars} />
        </NavButtonConatiner>
      )}
      {isNavBtnClicked && !loggedInUser && (
        <List>
          <ListItem>
            <Link to="/join">가입</Link>
          </ListItem>
          <ListItem>
            <Link to="/login">로그인</Link>
          </ListItem>
        </List>
      )}
      {isNavBtnClicked && loggedInUser && (
        <List>
          <ListItem>
            <Link to="/" exact={true}>
              <FontAwesomeIcon icon={faBasketShopping} />
              장보기
            </Link>
          </ListItem>
          <ListItem>
            <Link to="/my-fridge">냉장고</Link>
          </ListItem>
          <ListItem>
            <Link to="/profile">프로필</Link>
          </ListItem>
        </List>
      )}
    </Nav>
  );
};

export default Navigation;
