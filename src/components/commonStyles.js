import React from "react";
import styled from "styled-components";

export const Input = styled.input`
  min-width: 180px;
  width: 100%;
  background-color: ${({ theme, active }) =>
    active ? theme.lightGray : theme.black};
  color: ${({ theme, active }) => (active ? theme.black : theme.lightGray)};
  font-weight: bold;
  font-size: 1rem;
  padding: 5px 15px;
  border-radius: 15px;
`;

const InputContainer = styled.div`
  width: ${({ width }) => width};
  background-color: ${(props) => props.theme.lightGray};
  border-radius: 15px;
  padding: 2px 3px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;

const Button = styled.button`
  background-color: ${({ theme, active }) =>
    active ? theme.lightGray : theme.black};
  color: ${({ theme, active }) => (active ? theme.black : theme.lightGray)};
  font-weight: bold;
  font-size: 1rem;
  padding: 5px 15px;
  border-radius: 15px;
`;
export const RadiusButton = ({ inputValue, eventHandler, isValid }) => {
  return (
    <InputContainer>
      <Button
        isValid={isValid}
        onClick={() => (eventHandler ? eventHandler() : null)}
      >
        {inputValue}
      </Button>
    </InputContainer>
  );
};

const IconsContainer = styled.div`
  width: 30px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const Icon = ({ children }) => {
  return <IconsContainer>{children}</IconsContainer>;
};

const InputTypeCheckbox = styled.input.attrs({ type: "checkbox" })``;
const Label = styled.label`
  width: 50px;
  color: inherit;
`;

export const Checkbox = ({ item }) => {
  return (
    <>
      <InputTypeCheckbox type="checkbox" name={item.name} />
      <Label name={item.name}>{item.name}</Label>
    </>
  );
};
