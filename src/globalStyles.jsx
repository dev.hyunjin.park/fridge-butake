import { createGlobalStyle } from "styled-components";
const GlobalStyles = createGlobalStyle`
body {
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: ${({ theme }) => theme.black};
    touch-action: none;
}
* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    top: 40px;
}
li{
    list-style: none;
}
a { 
    text-decoration: none;
    text-decoration-style: none;
    color: inherit;
}
a:visited, a:active{
    color: inherit;
}
input, button {
    appearance: none;
    border: none;
    outline: none;
}
`;
export default GlobalStyles;
