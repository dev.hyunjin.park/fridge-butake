import React, { useState } from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPenToSquare,
  faShare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { Icon, Checkbox, Input } from "./commonStyles";

const ListContainer = styled.ul``;
const List = styled.li`
  height: 40px;
  margin: 0 15px;
  padding: 0 4px;
  display: flex;
  border-bottom: 1px solid ${({ theme }) => theme.black};
  align-items: center;
  transition: transform 200ms ease 0s;
  font-weight: 600;
  &:last-child {
    margin-bottom: 10px;
  }
`;
const EditForm = styled.form``;
const EditTextInput = styled.input.attrs({ type: "text" })``;
const ButtonsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: end;
  width: 90%;
`;
const AddItemForm = styled.form`
  padding: 0 20px;
`;
const SubmitBtn = styled.button`
  width: 70px;
  height: 35px;
  background-color: ${({ theme }) => theme.black};
  border-radius: 10px;
  color: ${({ theme }) => theme.lightGray};
  font-size: 1rem;
  font-weight: 600;
`;

const groceryData = [
  { name: "우유" },
  { name: "사과" },
  { name: "가지" },
  { name: "바나나" },
  { name: "양배추" },
];
const GroceryList = () => {
  const [lists, setLists] = useState(groceryData);
  const [grab, setGrab] = useState(null);
  const [dropTarget, setDropTarget] = useState(null);

  const [addInput, setAddInput] = useState("");
  const [editingTarget, setEditingTarget] = useState(null);
  const [editInputValue, setEditInputValue] = useState("");
  const [hideAddInput, setHideAddInput] = useState(true);

  const onDragStart = (e) => {
    setGrab(e.target);
  };
  // 드래깅 중인 요소: grab, 드랍 대상: dropTarget (dragOver의 마지막 타겟)
  const onDragOver = (e) => {
    e.preventDefault();
    if (e.target !== grab) {
      setDropTarget(e.currentTarget);
    }
  };
  const onDragEnd = (e) => {
    // dropTarget 요소의 height/2 윗쪽에서 놓아지면  --> dropTarget 윗쪽으로 이동
    e.preventDefault();
    const { top, bottom } = dropTarget.getBoundingClientRect();
    const height = bottom - top;
    const grabPos = +grab.dataset.position;
    const dropTargetPos = +dropTarget.dataset.position;
    setLists((current) => {
      if (grab === dropTarget) {
        return current;
      }
      const newLists = [...current];
      const deleted = newLists.splice(grabPos, 1);
      const deleteIndex =
        e.clientY < top + height / 2 ? dropTargetPos : dropTargetPos + 1;
      // grab의 초기 인덱스가 dropTarget의 인덱스보다 작을 경우
      // 드래그 된 아이템이 뒤로 빠지면서 인덱스는 하나씩 -1이 되기때문
      if (grab.dataset.position < dropTarget.dataset.position) {
        newLists.splice(deleteIndex - 1, 0, ...deleted);
      } else {
        newLists.splice(deleteIndex, 0, ...deleted);
      }
      return newLists;
    });
  };

  const handleAddItemSubmit = (e) => {
    e.preventDefault();
    setLists((current) => {
      const newLists = [...current, { name: addInput }];
      return newLists;
    });
    setAddInput("");
    setHideAddInput(true);
  };
  const handleEditBtn = (e, itemName) => {
    setEditingTarget(e.target.dataset.position);
    setEditInputValue(itemName);
  };
  const handleDeleteBtn = (e, index) => {
    console.log("delete");
    // TO DO
  };
  const handleAddItemInput = (e) => {
    setAddInput(e.target.value);
  };

  const handleEditSubmit = (e, index) => {
    e.preventDefault();
    setLists((current) => {
      const newLists = [...current];
      newLists[index] = { name: editInputValue };
      return newLists;
    });
    setEditingTarget(null);
  };
  const handleEditInput = (e) => {
    setEditInputValue(e.target.value);
  };

  return (
    <>
      <ListContainer>
        {lists.map((item, index) => (
          <List
            key={index + item}
            data-position={index}
            onDragOver={onDragOver}
            onDragStart={onDragStart}
            onDragEnd={onDragEnd}
            draggable="true"
          >
            <>
              {editingTarget !== index + "" && <Checkbox item={item} />}
              {editingTarget && editingTarget === index + "" && (
                <>
                  <EditForm onSubmit={(e) => handleEditSubmit(e, index)}>
                    <EditTextInput
                      type="text"
                      data-position={index}
                      value={editInputValue}
                      onChange={handleEditInput}
                    />
                    <Icon>
                      <FontAwesomeIcon icon={faShare} />
                    </Icon>
                  </EditForm>
                  <Icon>
                    <FontAwesomeIcon icon={faTrash} />
                  </Icon>
                </>
              )}
            </>
            {!editingTarget && (
              <ButtonsContainer>
                <Icon>
                  <FontAwesomeIcon
                    icon={faPenToSquare}
                    onClick={(e) => handleEditBtn(e, item.name)}
                  />
                </Icon>
                <Icon>
                  <FontAwesomeIcon
                    icon={faTrash}
                    onClick={(e) => handleDeleteBtn(e, index)}
                  />
                </Icon>
              </ButtonsContainer>
            )}
          </List>
        ))}
      </ListContainer>
      <AddItemForm onSubmit={handleAddItemSubmit}>
        {!hideAddInput && (
          <Input
            type="text"
            placeholder="아이템 추가"
            value={addInput}
            onChange={handleAddItemInput}
          />
        )}
        {hideAddInput && (
          <SubmitBtn
            onClick={() => setHideAddInput((current) => !current)}
            type="submit"
          >
            추가 +
          </SubmitBtn>
        )}
      </AddItemForm>
    </>
  );
};

export default GroceryList;
