import React, { useState } from "react";
import styled from "styled-components";

const data = [
  { name: "토마토", amount: 9 },
  { name: "가지", amount: 9 },
  { name: "치즈", amount: 5 },
  { name: "감자", amount: 2 },
  { name: "우유", amount: 1 },
  { name: "양배추", amount: 7 },
  { name: "양파", amount: 5 },
];

const CustomInputRange = styled.input.attrs({ type: "range" })`
  background: transparent;
  &:focus {
    outline: none;
  }
  &::-webkit-slider-thumb {
    /* 크롬 thumb 스타일 */
    -webkit-appearance: none;
    background: ${({ theme }) => theme.lightGray};
    height: 12px;
    width: 12px;
    border-radius: 10px;
    box-shadow: 1px 1px 1px ${({ theme }) => theme.black},
      0px 0px 1px ${({ theme }) => theme.black};
    margin-top: -4px; // thumb 위치 조절
    cursor: pointer;
  }
  &::-webkit-slider-runnable-track {
    /* 크롬 bar 스타일 */
    width: 100%;
    height: 4px;
    cursor: pointer;
    box-shadow: 1px 1px 1px ${({ theme }) => theme.black},
      0px 0px 1px ${({ theme }) => theme.black};
    background: ${({ theme }) => theme.lightGray};
    border-radius: 1.3px;
  }
`;
const List = styled.ul`
  margin: 10px 15px;
  padding: 0 4px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;
const ListItem = styled.li`
  width: 70%;
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-bottom: 10px;
`;
const Label = styled.label`
  font-weight: 600;
`;

const InventoryList = () => {
  const [lists, setLists] = useState(data);
  const handleRange = (e, index) => {
    setLists((current) => {
      const newLists = [...current];
      newLists.forEach((item, itemIndex) => {
        if (itemIndex === index) {
          item.amount = e.target.value;
        }
      });
      return newLists;
    });
  };
  return (
    <>
      <List>
        {lists.map((item, index) => (
          <ListItem key={index}>
            <Label>{item.name}</Label>
            <CustomInputRange
              data-position={index}
              type="range"
              onChange={(e) => handleRange(e, index)}
              min={0}
              max={10}
              value={item.mount}
            />
          </ListItem>
        ))}
      </List>
    </>
  );
};

export default InventoryList;
