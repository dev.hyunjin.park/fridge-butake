import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { theme } from "./utils/theme";
import GlobalStyles from "./globalStyles";
import InventoryList from "./components/InventoryList";
import Navigation from "./components/Navigation";
import Login from "./components/Login";
import Join from "./components/Join";
import MainScreen from "./components/MainScreen";
import Recipe from "./components/Recipe";

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <BrowserRouter>
          <Switch>
            <Route path="/" exact={true}>
              <MainScreen />
            </Route>
            <Route path="/join" component={Join} />
            <Route path="/login" component={Login} />
            {/* <Route path="/profile"  component={Profile} /> */}
            <Route path="/my-fridge" component={InventoryList} />
            <Route path="/recipe/:id" component={Recipe} />
            <Redirect to="/" />
          </Switch>
          <Navigation />
        </BrowserRouter>
      </ThemeProvider>
    </div>
  );
}

export default App;
