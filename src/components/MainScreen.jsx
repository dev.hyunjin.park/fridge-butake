import React, { useState } from "react";
import styled from "styled-components";
import { globalVariables } from "../utils/constants";
import RecommendationList from "./RecommendationList";
import GroceryList from "./GroceryList";
import InventoryList from "./InventoryList";

const toggleBtnWidth = "7";

const WhiteBoard = styled.div`
  margin: 10px 20px;
  padding: 10px 0;
  background-color: ${({ theme }) => theme.lightGray};
  border-radius: ${globalVariables.borderRadius};
`;
const ToggleBtnContainer = styled.div`
  display: flex;
  justify-content: end;
  padding: 10px 20px 0 0;
`;
const BtnBackground = styled.div`
  border-radius: 32px;
  width: ${toggleBtnWidth}rem;
  height: 2rem;
  background-color: ${({ theme }) => theme.lightGray};
`;
const NameTag = styled.div`
  position: absolute;
  top: 10px;
  width: ${toggleBtnWidth}rem;
  height: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1rem;
  font-weight: 600;
`;
const Name = styled.div`
  &:first-child {
    padding-left: 0.5rem;
  }
  &:last-child {
    padding-right: 0.5rem;
  }
`;
const ToggleBtn = styled.input`
  position: relative;
  top: 0;
  appearance: none;
  border: max(2px, 0.1rem) solid ${({ theme }) => theme.lightGray};
  border-radius: 1.4rem;
  width: ${toggleBtnWidth}rem;
  height: 2rem;
  &:before {
    position: absolute;
    content: "장보기";
    color: ${({ theme }) => theme.lightGray};
    font-size: 1.2rem;
    font-weight: 500;
    padding: 0 0.2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    top: -0.25rem;
    left: -0.4rem;
    width: 60%;
    height: 2.25rem;
    border-radius: 1.4rem;
    transform: scale(0.8);
    background-color: ${({ theme }) => theme.black};
    transition: left 250ms linear;
  }
  &:checked:before {
    position: absolute;
    content: "냉장고";
    background-color: ${({ theme }) => theme.black};
    left: ${toggleBtnWidth / 2 - 0.8}rem;
    color: ${({ theme }) => theme.lightGray};
    font-size: 1.2rem;
    font-weight: 500;
    padding: 0 0.2rem;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

const MainScreen = () => {
  const [toggleMain, setToggleMain] = useState(true);
  const handleComplete = () => {
    // 체크박스 체크된 아이템 모두 불러오기
    // 냉장고로 이동
    // 아이템에 각 amount 저장 되어야 함
  };

  return (
    <>
      <ToggleBtnContainer>
        <BtnBackground>
          <NameTag>
            <Name>장보기</Name>
            <Name>냉장고</Name>
          </NameTag>
          <ToggleBtn
            role="switch"
            type="checkbox"
            onChange={(e) => setToggleMain((current) => !current)}
          />
        </BtnBackground>
      </ToggleBtnContainer>
      <WhiteBoard>
        {toggleMain ? <GroceryList /> : <InventoryList />}
      </WhiteBoard>
      <RecommendationList />
    </>
  );
};

export default MainScreen;
